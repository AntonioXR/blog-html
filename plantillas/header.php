<head>
  <!-- PAGINA REALIZADA POR ANTONIO XOCOY ROSALES -->
  <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Architects+Daughter|Josefin+Sans:100|Josefin+Slab|Orbitron|Playfair+Display" rel="stylesheet">
  <script  src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="resources/css/styles.css" media="screen" title="no title" charset="utf-8">
  <meta charset="utf-8">
  <link rel="shortcut icon" href="resources/img/logoPagina.jpg" >
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Blog Antonio Xocoy Rosales</title>
</head>
