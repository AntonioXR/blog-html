
CREATE DATABASE blog_in6av;
USE blog_in6av;

CREATE TABLE roles(
  idRol INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  rol VARCHAR(20) NOT NULL
);
CREATE TABLE usuario(
    idUsuario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(40) NOT NULL,
    apellido VARCHAR(40) NOT NULL,
    nick VARCHAR(15) NOT NULL,
    contrasena VARCHAR(10) NOT NULL,
    idRol INT NOT NULL,
    FOREIGN KEY(idRol) REFERENCES roles(idRol)
);
CREATE TABLE categoria(
  idCategoria INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  categoria VARCHAR(20) NOT NULL
);
CREATE TABLE estadoPost(
  idEstado INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  estado VARCHAR(20) NOT NULL
);
CREATE TABLE post(
  idPost INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  idCategoria INT NOT NULL,
  idUsuario INT NOT NULL,
  idEstadoP INT NOT NULL,
  titulo VARCHAR(70) NOT NULL,
  contenido TEXT NOT NULL,
  fecha DATETIME NOT NULL,
  FOREIGN KEY (idCategoria) REFERENCES categoria(idCategoria),
  FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario),
  FOREIGN KEY (idEstadoP) REFERENCES estadoPost(idEstado)
);
CREATE TABLE suscriptores(
  idSuscripcion INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  idUsuarioPublica INT NOT NULL,
  idUsuarioLector INT NOT NULL,
  FOREIGN KEY (idUsuarioPublica) REFERENCES usuario(idUsuario),
  FOREIGN KEY (idUsuarioLector) REFERENCES usuario(idUsuario)
);
/*  ------------------INICIO INSERCIONES A TABLAS ------------------ */
INSERT INTO roles(rol)
VALUES ('Administrador'),('escritor'),('visor');

INSERT INTO estadoPost(estado)
VALUES ('publicado'),('borrador');

INSERT INTO categoria(categoria)
VALUES ('Romantico'),('Anegdota'),('Musica'),('Moda');

INSERT INTO `usuario` (`idUsuario`, `nombre`, `apellido`, `nick`, `contrasena`,`idRol`) VALUES
(1, 'Jose Antonio ', 'Xocoy Rosales', 'tonioros', '1234',1),
(2, 'Lesly', 'Raxon', 'lesRax', '12345',2);

INSERT INTO post(idCategoria, idUsuario, idEstadoP,titulo ,contenido, fecha)
VALUES (1,1,2,'Primer Post','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
', NOW());
INSERT INTO suscriptores(idUsuarioPublica,idUsuarioLector)
VALUES (1,2);
