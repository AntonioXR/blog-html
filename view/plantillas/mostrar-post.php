<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('America/Guatemala');
$dato = $_SESSION['datosUsuario'];
$dataUser = $dato[0]; ?>
<div class="container-fluid" style="margin-top: 9px">
  <button onclick="cambioCentro(2)" type="button" class="btn btn-primary" name="button"><span class="glyphicon glyphicon-arrow-left" arial-hidden="true"></span> Volver</button>
  <h2>Aca se muestra tu historial de publicaciones</h2>
  <div class="row" style="margin-top: 1.5em;">
      <h3>Borradores</h3>
    <div id="caja-borradores" class="col-lg-12">
    </div>
    <button type="button" id="btn-masBorradores" class="btn " name="button" onclick="MostarMasReg(2)"><span class="glyphicon glyphicon-plus" arial-hidden="true" ></span> Mostrar más borradores</button>
  </div>
  <div class="row" style="margin-top: 1.5em; margin-bottom: 2.5em;">
      <h3>Post publicados</h3>
    <div id="caja-publicados" style="margin-top: 4px;margin-bottom: 4px;" class="col-lg-12">
    </div>
    <button type="button" class="btn " id="btn-masPublicacion" name="button" onclick="MostarMasReg(1)"><span class="glyphicon glyphicon-plus" arial-hidden="true" ></span> Mostrar más publicaciones</button>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js" charset="utf-8"></script>
<script type="text/javascript">
var opcionesNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'success', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
};
var errorNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'error', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
};
  $.ajax({
    method: 'POST',
    url: '../../controller/post.php',
    data: {
      'opcion': 'mostrar-p-user-5',
      'idUsuario': <?php echo $dataUser['idUsuario']; ?>
    }
  }).done(function(resp){
    var respuesta = JSON.parse(resp);
    if(respuesta != false || respuesta!= null){
      for (var i = 0; i < respuesta.length; i++) {
        var ingreso = '<div class="caja-post" ><h3 class="titulo-p">'+respuesta[i].titulo+'</h3><h4 class="subtitulo-p">|Categoria: '+respuesta[i].categoria+'</h4><h4 class="subtitulo-p">|Fecha Creador:'+respuesta[i].fecha+' </h4><div class="clearfix"></div><p>'+respuesta[i].contenido+'</p></div>';
        if(respuesta[i].idEstadoP == 1){
          $('#caja-publicados').append(ingreso);
        }else{
            ingreso = '<div class="caja-post"><h3 class="titulo-p">'+respuesta[i].titulo+'</h3><h4 class="subtitulo-p">|Categoria: '+respuesta[i].categoria+'</h4><h4 class="subtitulo-p">|Fecha Creador:'+respuesta[i].fecha+' </h4><div class="clearfix"></div><p>'+respuesta[i].contenido+'</p>    <button type="button" class="btn btn-success pull-rigth" name="button"  onclick="cambioAPub('+respuesta[i].idPost+')">Publicar borrador</button></div>';
          $('#caja-borradores').append(ingreso)
        }
      }
    }
  });

  function MostarMasReg(estadoP){
    $.ajax({
      method: 'POST',
      url: '../../controller/post.php',
      data: {
        'opcion': 'mostrar-p-user',
        'estadoP': estadoP,
        'idUsuario': <?php echo $dataUser['idUsuario']; ?>
      }
    }).done(function(resp){
      if (estadoP == 1) {
        $('#caja-publicados').empty();
        $('#caja-publicados').html("");
        $('#caja-publicados').addClass("pre-scrollable");
      }else if (estadoP == 2){
        $('#caja-borradores').empty();
        $('#caja-borradores').html("");
        $('#caja-borradores').addClass("pre-scrollable");
      }
      var respuesta = JSON.parse(resp);
      if(respuesta != false || respuesta!= null){
        for (var i = 0; i < respuesta.length; i++) {
          var ingreso = '<div class="caja-post" ><h3 class="titulo-p">'+respuesta[i].titulo+'</h3><h4 class="subtitulo-p">|Categoria: '+respuesta[i].categoria+'</h4><h4 class="subtitulo-p">|Fecha Creador:'+respuesta[i].fecha+' </h4><div class="clearfix"></div><p>'+respuesta[i].contenido+'</p></div>';
          if(respuesta[i].idEstadoP == 1){
            $('#caja-publicados').append(ingreso);
          }else{
              ingreso = '<div class="caja-post"><h3 class="titulo-p">'+respuesta[i].titulo+'</h3><h4 class="subtitulo-p">|Categoria: '+respuesta[i].categoria+'</h4><h4 class="subtitulo-p">|Fecha Creador:'+respuesta[i].fecha+' </h4><div class="clearfix"></div><p>'+respuesta[i].contenido+'</p>    <button type="button" class="btn btn-success pull-rigth" name="button" onclick="cambioAPub('+respuesta[i].idPost+')">Publicar borrador</button></div>';
            $('#caja-borradores').append(ingreso)
          }
        }
      }
    });
  }
  function cambioAPub(idPost) {
    $.ajax({
      method: 'POST',
      url: '../../controller/post.php',
      data: {
        'opcion': 'borrador-a-p',
        'idPost': idPost
      }
    }).done(function(resp){
      var respuesta = JSON.parse(resp);
      if(respuesta==true){
        $.notify('Se cambio el Estado de Borrador a Post',opcionesNotify );
      }else {
        $.notify('Ocurrio un error en el cambio, recargue la pagina ',errorNotify );
      }
      MostarMasReg(2);
      MostarMasReg(1);
    });
  }
</script>
