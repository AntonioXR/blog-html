<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', '1');
$dato = $_SESSION['datosUsuario'];
$dataUser = $dato[0];
date_default_timezone_set('America/Guatemala');
include '../../model/use-cURL.php';?>
<!-- PANEL DE NUEVA ENTRADA -->

<div class="container-fluid">
  <div class="publicar-p-parallax">
    <br><br>
  <h2 class="text-center" style="border-bottom: double 2px #ffffff ;font-family: 'Raleway', sans-serif; color: #fff;">¿Tienes algun pensamiento nuevo? <br> <b>¡Compartelo!</b> <br> <br> Tus suscriptores te esperan</h2>
    <br>
      <br>
    </div>
      <div  class="col-lg-8">
      <button type="button" style="margin-left: 45%;font-size: 1.5em; margin-top: 10px; margin-bottom: 6px ; margin-rigth: 45%;" name="button" class="btn " onclick="cambioCentro(1)"><span class=" glyphicon glyphicon-list-alt" arial-hidden="true"></span> Ver tus borradores y Post publicados</button>
      <h2 class="text-center" style="color: #000;">¿Cúal será el titulo de tu nueva entrada?</h2>
        <input type="text" id="titulo-p" placeholder="Titulo..." class="form-control text-center" style="margin-left: 30%;margin-rigth: 30%; margin-top: 2em;height: 40px; font-size: 1.3em;" required>
    </div>
    <br>
    <div style="clear: both;"></div>
    <h2 class="text-center">Expresa como te sientes, comparte lo a tus suscriptores</h2>
  <div class="row">
    <div class="col-lg-11" style="margin-top: 2em;">
      <textarea id="contenido-p" rows="8" style="margin-left: 5%;margin-rigth: 5%;" class="form-control" cols="80" placeholder="Escribe lo que piensas aca..." required></textarea>
    </div>
    <div style="clear: both;"></div>
    <br>
    <h2 class="text-center">¿Qué categoria le daras a tu entrada?</h2>
    <div class="col-lg-8" style="margin-top: 3em; margin-bottom: 3em;">
      <select id="categorias" class="form-control" style="margin-left: 30%;margin-rigth: 30%;height: 40px; font-size: 1.3em;" required>
        <?php
        $datos = array('opcion' => 'mostrar-c');
        $resultado = json_decode(useCURL('http://localhost/blog_in6av/controller/categoria.php', $datos));
        if($resultado != null){
          foreach ($resultado as $row){
              echo "<option value=".$row->idCategoria.">".$row->categoria."</option>" ;
          }
        }else{
          echo "<option value=''>Error al cargar, recarge la pagina</option>" ;
        }
         ?>
      </select>
    </div>
    <div style="clear: both;"></div>
    <h3>Ahora, tienes dos opciones</h3>
    <ul>
      <li class="h3">Publicar tu entrada a todo publico</li>
      <li class="h3">Mantener como borrador tu entrada y publicarlo hasta que decees</li>
    </ul>
  </div>
  <div class="row">
    <div class="col-sm-9"  style="margin-top: 2%; margin-bottom: 4%;;margin-left: 30%;margin-rigth: 30%;font-size: 1.3em;">
      <button onclick="SendData(1)" id="publicar-p" type="button" class="btn btn-info h1 pull-rigth">Publicar entrada</button>
      <button onclick="SendData(2)" id="borrador-p" type="button" class="btn btn-info h1">Matener como borrador</button>
    </div>
  </div>
  </div>
  <script type="text/javascript">
  function SendData(estadoP){
    if($('#contenido-p').val() == "" && $('#titulo-p').val() == ""){
        $.notify("Por favor no dejar areas incompletas",errorNotify);
    }else{
    $.ajax({
      url:'../../controller/post.php',
      method: 'POST',
      data: {
        opcion: 'crear-p',
        idCategoria: $('#categorias').val(),
        idUsuario: <?php echo $dataUser['idUsuario']; ?>,
        idEstadoP: estadoP,
        contenido: $('#contenido-p').val(),
        titulo: $('#titulo-p').val(),
        fecha: hoy
      }
    }).done(function(respuesta) {
      var data = JSON.parse(respuesta);
      if(data==true){
      var data = JSON.parse(respuesta);
      if(data && estadoP==1){
        $.notify("Publico su post con exito",opcionesNotify);
      }else if(data && estadoP ==2){
        $.notify("Se almaceno en sus borradores con exito",opcionesNotify)
      }
      $('#contenido-p').val("");
      $('#titulo-p').val("");
    }else{
        $.notify("Ocurrio un error en la operacion, recargue la pagina por favor",errorNotify);
        console.log(respuesta);
    }
    });
  }
}
  </script>
