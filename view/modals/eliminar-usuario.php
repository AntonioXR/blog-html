<div class="modal fade" id="Modal-el-usuario" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-el-u"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title text-center" style="font-family: 'Josefin Slab', cursive;">Eliminar Usuario</h2>
      </div>
      <div class="modal-body">
        <h3 id="text-delete-u"></h3>
      <button onclick="eliminarU()" type="button" class="btn btn-danger" name="button">Eliminar</button>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js" charset="utf-8"></script>
<script type="text/javascript">
function eliminarUser(idUsuario) {
  idUsuarioEdit = idUsuario;
  $.ajax({
    method: 'POST',
    url: '../../controller/usuario.php',
    data:{
      opcion: 'unUsuario',
      'id-u': idUsuario
    }
  }).done(function(respuesta) {
    var res = JSON.parse(respuesta);
    $('#text-delete-u').text("Desea eliminar al Usuario "+res[0].nombre+" ");
  });
}
function eliminarU() {
  $.ajax({
    method: 'POST',
    url: '../../controller/usuario.php',
    data:{
      opcion: 'eliminar-u',
      'id-u': idUsuarioEdit
    }
  }).done(function(respuesta) {
    var res = JSON.parse(respuesta);
    if(res==true){
        $.notify("Elimino el usuario con exito",opcionesNotify);
        cargarListaUS();
        $('#close-el-u').clik();
    }else {
        $.notify("Ocurrio un error recargue la pagina",errorNotify);
        cargarListaUS();
          $('#close-el-u').clik();
    }
  });
}
</script>
