<div class="modal fade" id="Modal-ag-usuario" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-agregar-u"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title text-center" style="font-family: 'Josefin Slab', cursive;">Usuario nuevo</h2>
      </div>
      <div class="modal-body">
        <img src="../../resources/img/nuevo-usuario.jpg" alt="Nuevo usuario" style="margin-left: 40%; margin-rigth: 40%;height: 155px;">
        <form  action="http://localhost/blog_in6av/controller/usuario.php" method="post">
            <input style="margin-top: 5px;" placeholder="Nombre" class="form-control" id="nombre-u" type="text" name="nombre-u" value="">
            <input placeholder="Apelido" style="margin-top: 5px;"  class="form-control" id="apellido-u" type="text" name="apellido-u" value="">
            <input  placeholder="Nick" style="margin-top: 5px;"  class="form-control" id="nick-u" type="text" name="nick-u" value="">
            <input placeholder="Contraseña" style="margin-top: 5px;"  class="form-control" id="passw-u" type="password" name="passw-u" value="">
            <input placeholder="Repita Contraseña" style="margin-top: 5px;"  class="form-control" id="pass2-u" type="password" name="pass2-u" value="">
            <select style="margin-top: 5px;"  class="form-control" id="idRol-u" name="idRol-u" >
              <option value="1">Administrador</option>
              <option value="2">Escritor</option>
              <option value="3">Lector</option>
            </select>
            <button style="margin-top: 5px; float: rigth;"  onclick="agregarUser()" class="btn btn-primary " type="button" >Agregar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js" charset="utf-8"></script>
<script type="text/javascript">
var opcionesNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'success', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
};
var errorNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'error', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
};
function agregarUser() {
  if($('#nombre-u').val()== "" && $('#apellido-u').val() == "" && $('#nick-u').val() =="" && $('#passw-u').val()==""){
    $.notify("Complete los campos por favor",errorNotify);
  }else {
    console.log($('#passw-u').val() + $('#pass2-u').val());
    if($('#passw-u').val() == $('#pass2-u').val()){
      $.ajax({
        method: 'POST',
        url: '../../controller/usuario.php',
        data:{
          'opcion': 'agregar-u',
          'nombre-u': $('#nombre-u').val(),
          'apellido-u': $('#apellido-u').val(),
          'nick-u': $('#nick-u').val(),
          'passw-u': $('#passw-u').val(),
          'idRol-u': $('#idRol-u').val()
        }
      }).done(function(respuesta) {
          var resp = JSON.parse(respuesta);
          console.log(respuesta);
          if(resp==true){
            $.notify("Publico su post con exito",opcionesNotify);
            $('#nombre-u').val("");
            $('#apellido-u').val("");
            $('#nick-u').val("");
            $('#passw-u').val("");
            $('#idRol-u').val("");
            $('#close-agregar-u').click();
          }
          cargarListaUS();
      });
    }else{
      $.notify("Las contraseñas no son iguales, verifiquelas por favor",errorNotify);
    }
  }
}
</script>
