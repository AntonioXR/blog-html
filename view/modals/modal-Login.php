<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" style="font-family: 'Josefin Slab', cursive;">Ingresa tu usuario y tu contraseña</h2>
      </div>
      <div class="modal-body">
        <img style="margin-left: 35%; margin-rigth: 35%; height: 170px; margin-bottom: 2em; " src="resources/img/avatar-login.png" alt="" />
       <div class="container-fluid">
          <input id="usuario" type="text"  name="nick" required="true" style="font-size: 1.2em;margin: 5px;" placeholder="Usuario" class="form-control" name="user" value="">
          <input  id="passw" type="password" name="contrasena" required="true" style="margin: 5px; font-size: 1.2em" placeholder="Contraseña" class="form-control" name="password" value="">
          <input  type="hidden" name="opcion" value="login">
          <input onclick="Login()" class="btn btn-primary " type="submit" name="envio"  style="margin: 5px; font-size: 1.2em; float: right;" value="Ingresar">
          <div style="clear: both;"></div>
          <div class="clearfix"></div>
          <div id="fallo">

          </div>
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function Login() {
    $.ajax({
      url:'http://localhost/blog_in6av/controller/usuario.php',
      method: 'post',
      data: {
          nick: $('#usuario').val(),
          contrasena: $('#passw').val(),
          opcion: 'login'
      }
    }).done(function (respuesta) {
      var resp = JSON.parse(respuesta);
    console.log(respuesta);
      if(resp.location ==false){
        $('#fallo').html('');
          $('#fallo').append('<div class="alert alert-warning alert-dismissible" role="alert" <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>¡Usuario Incorrecto!</strong> verifique el usuario y la contraseña      </div>');
      }else {
        window.locationf=resp.location;
      }
    });
  }
</script>
