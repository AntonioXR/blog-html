<div class="modal fade" id="Modal-ed-usuario" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-edit-u"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title text-center" style="font-family: 'Josefin Slab', cursive;">Editar Usuario</h2>
      </div>
      <div class="modal-body">
        <img src="../../resources/img/nuevo-usuario.jpg" alt="Nuevo usuario" style="margin-left: 40%; margin-rigth: 40%;height: 155px;">
        <form  action="http://localhost/blog_in6av/controller/usuario.php" method="post">
            <input style="margin-top: 5px;" placeholder="Nombre" class="form-control" id="nombre-u-edit" type="text" name="nombre-u" value="">
            <input placeholder="Apelido" style="margin-top: 5px;"  class="form-control" id="apellido-u-edit" type="text" name="apellido-u" value="">
            <input  placeholder="Nick" style="margin-top: 5px;"  class="form-control" id="nick-u-edit" type="text" name="nick-u" value="">
            <input placeholder="Contraseña" style="margin-top: 5px;"  class="form-control" id="passw-u-edit" type="password" name="passw-u" value="">
            <input placeholder="Repita Contraseña" style="margin-top: 5px;"  class="form-control" id="pass2-u-edit" type="password" name="pass2-u" value="">
            <select style="margin-top: 5px;"  class="form-control" id="idRol-u-edit" name="idRol-u" >
              <option value="1">Administrador</option>
              <option value="2">Escritor</option>
              <option value="3">Lector</option>
            </select>
            <button style="margin-top: 5px; float: rigth;"  onclick="editUser()" class="btn btn-primary " type="button" >Agregar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js" charset="utf-8"></script>
<script type="text/javascript">

var idUsuarioEdit= 0;
var opcionesNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'success', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
};
var errorNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'error', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
};
function editUser() {
  if($('#nombre-u-edit').val()== "" && $('#apellido-u-edit').val() == "" && $('#nick-u-edit').val() =="" && $('#passw-u-edit').val()==""){
    $.notify("Complete los campos por favor",errorNotify);
  }else {
    if($('#passw-u-edit').val() == $('#pass2-u-edit').val()){
      $.ajax({
        method: 'POST',
        url: '../../controller/usuario.php',
        data:{
          'opcion': 'editar-u',
          'id-u': idUsuarioEdit,
          'nombre-u': $('#nombre-u-edit').val(),
          'apellido-u': $('#apellido-u-edit').val(),
          'nick-u': $('#nick-u-edit').val(),
          'passw-u': $('#passw-u-edit').val(),
          'idRol-u': $('#idRol-u-edit').val()
        }
      }).done(function(respuesta) {
          var resp = JSON.parse(respuesta);
          console.log(respuesta);
          if(resp==true){
            $.notify("Publico su post con exito",opcionesNotify);
            $('#nombre-u').val("");
            $('#apellido-u').val("");
            $('#nick-u').val("");
            $('#passw-u').val("");
            $('#idRol-u').val("");
            $('#close-edit-u').click();
            cargarListaUS()
          }
      });
    }else{
      $.notify("Las contraseñas no son iguales, verifiquelas por favor",errorNotify);
    }
  }
}

function editarUsuarioF(idUsuario) {
  idUsuarioEdit = idUsuario;
  $.ajax({
    method: 'POST',
    url: '../../controller/usuario.php',
    data:{
      opcion: 'unUsuario',
      'id-u': idUsuario
    }
  }).done(function(respuesta) {
    var res = JSON.parse(respuesta);
    console.log(res);
  $('#nombre-u-edit').val(res[0].nombre);
  $('#apellido-u-edit').val(res[0].apellido);
  $('#nick-u-edit').val(res[0].nick);
  $('#passw-u-edit').val(res[0].contrasena);
   $('#pass2-u').val(res[0].contrasena);
  $('#idRol-u-edit').val(res[0].idRol);
  })
}
</script>
