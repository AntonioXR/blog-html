<?php session_start();
error_reporting(E_ALL);
ini_set('display_errors', '1');?>
<!DOCTYPE html>
<html>
   <?php
   $dato = $_SESSION['datosUsuario'];
   $dataUser = $dato[0];
    include '../plantillas/head.php'; ?>
  <body>
    <?php include '../plantillas/header-nav.php';
          include '../../model/use-cURL.php';
          include '../modals/agregar-usuario.php';
          include '../modals/editar-usuario.php';
          include '../modals/eliminar-usuario.php';?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 pre-scrollable">
              <!-- Panel de usuarios -->
              <?php $datos = array('opcion' =>'getNicks','idUsuario' => $dataUser['idUsuario']);
                    $usuario= json_decode(useCURL('http://localhost/blog_in6av/controller/usuario.php',$datos));
                    $conteoUser =  json_decode(useCURL('http://localhost/blog_in6av/controller/usuario.php',array('opcion' =>'NoUsuarios','idUsuario' => $dataUser['idUsuario'])));
                if ((isset($usuario) && isset($conteoUser)) ||($usuario != null && $conteoUser != null)){
                  echo "<h3>Tus usuarios (".$conteoUser[0]->conteo.")</h3>";
                    foreach ($usuario as $row){?>
                <div style="padding: 2px; border: double 1.3px rgb(228, 188, 67); border-radius: 2px; margin: 3px;">
                      <h4>Nombre: <?php echo $row->nombre ?></h4>
                      <h4>Usuario: <?php echo $row->nick ?></h4>
                      <h4>Tipo U.: <?php echo $row->rol ?></h4>
                </div><?php }} ?>
            </div>
            <div class="col-lg-9">
              <div style="clear: both;"></div>
                <div class="admin-t-parallax">
                  <br>
                  <br>
                  <br>
                    <h1 class="text-center" style="font-family: 'Raleway', sans-serif; color: #000000; font-size: 3em;">Bienvenido, ¿Qué haremos hoy?</h1>
                    <h3 class="text-center" style="border-bottom: double 2px #000000 ;font-family: 'Raleway', sans-serif; color: #000000;">Abajo tienes tu panel de gestion de usuarios</h3>
                </div>
                <div style="clear: both;"></div>
                <div style="margin-top: 1.8em; margin-bottom: 2em;" >
                  <h3 class="text-center">¿Usuario nuevo?, agregalo, pulsa el boton de abajo y completa el formulario</h3>
                  <button data-toggle="modal" data-target="#Modal-ag-usuario" type="button" style="margin-left: 40%; margin-rigth: 40%; font-size:1.5em; margin-top: 2em;  " class="btn btn-success" name="button">Agregar Usuario</button>
                </div>
              <table style="background: #fff;" class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th><b>Nombre</b></th>
                    <th><b>Apellido</b></th>
                    <th><b>Usuario</b></th>
                    <th><b>Tipo Usuario</b></th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                  </tr>
                </thead>
                <tbody id="tBody-usuarios">

                </tbody>
              </table>
            </div>
        </div>
    </div>
  </body>
  <script type="text/javascript">
  function cargarListaUS(){
    $.ajax({
      method:'POST',
      url: '../../controller/usuario.php',
      data:{
        opcion: 'getNicks',
        idUsuario: <?php echo $dataUser['idUsuario']; ?>
      }
    }).done(function(resp) {
       var respuesta = JSON.parse(resp);
       $('#tBody-usuarios').empty();
       $('#tBody-usuarios').html("");
       for (var i = 0; i < respuesta.length; i++) {
         var ingreso ="<tr><th>"+respuesta[i].nombre+"</th><th>"+respuesta[i].apellido+"</th><th>"+respuesta[i].nick+"</th><th>"+respuesta[i].rol+"</th><th><button data-toggle='modal' data-target='#Modal-ed-usuario' onclick='editarUsuarioF("+respuesta[i].idUsuario+")' type='button' name='button'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></button></th><th><button type='button' name='button'><span class='glyphicon glyphicon-trash' aria-hidden='true'  data-toggle='modal' data-target='#Modal-el-usuario' onclick='eliminarUser("+respuesta[i].idUsuario+")'></span></button></th></tr>";
         $('#tBody-usuarios').append(ingreso);
       }
    });
  }
  cargarListaUS();
  </script>
</html>
