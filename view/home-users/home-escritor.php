<?php session_start();
error_reporting(E_ALL);
date_default_timezone_set('America/Guatemala');
ini_set('display_errors', '1');?>
<!DOCTYPE html>
<html>
  <?php include '../plantillas/head.php'; ?>
  <body>
  <?php include '../plantillas/header-nav.php';
        include '../../model/use-cURL.php';?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3 pre-scrollable" style="background: #fff;max-height: 57em;">
        <!-- SE ALMACENARAN LAS PERSONAS SUSCRITAS AL REDACTOR -->
        <?php $data1 = $_SESSION['datosUsuario'];
              $datosUser = $data1[0];
              $datos = array('opcion' => 'mis-suscripciones', 'idUsuarioLector' =>  $datosUser['idUsuario'] );
              $return = json_decode(useCURL( 'http://localhost/blog_in6av/controller/suscricion.php',$datos));
              if($return != null || $return != ""){
                foreach ($return as $row) {
              ?>
                <div class="caja-suscriptores" >
                  <h3><?php echo $row['nick']; ?></h3>
                  <h4><?php echo $row['nombre']; ?></h4>
                </div>
          <?php }
        }else {
          echo "<div class='caja-suscriptores' >
                <h2>Aun no tienes suscriptores :c </h2>
          </div>";
        } ?>
      </div>
      <div id="caja-ejecucion" style="background: #fff;" class="col-md-8">

      </div>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js" charset="utf-8"></script>
  <script type="text/javascript">
   var hoy = <?php echo "'".date('Y-m-d H:m:s')."';"; ?>
   var opcionesNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'success', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
  };
  var errorNotify = {clickToHide: true,autoHide: true,autoHideDelay: 5000,arrowShow: true,arrowSize: 100, position: '...', elementPosition: 'top center',globalPosition: 'top center',style: 'bootstrap', className: 'error', showAnimation: 'slideDown', showDuration: 400, hideAnimation:'slideUp', hideDuration: 200, gap: 2
 };
 function cambioCentro(cambiarA) {
   $('#caja-ejecucion').empty();
   $('#caja-ejecucion').html("");
   switch (cambiarA) {
     case 1:
     $('#caja-ejecucion').load('../plantillas/mostrar-post.php');
       break;
     case 2:
     $('#caja-ejecucion').load('../plantillas/crear-post.php');
       break;
     default:
   }
 }

 cambioCentro(2);
  </script>
  </body>
</html>
