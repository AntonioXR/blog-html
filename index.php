<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <?php include 'view/plantillas/head.php'; ?>
  <body>
    <!-- CREACION DEL HEADER  -->
      <?php include 'view/modals/modal-Login.php';
            include 'view/plantillas/header-nav.php';?>

    <div style="clear: both;"></div>
    <!-- FINALIZACION DEL HEADER, INICIO DEL CUERPO  -->
    <br>
    <h1 class="text-center" style="font-family: 'Josefin Sans'; font-size: 5em;">Publica, comparte, y obten subscriptores. <br>
        Aca en <b>Blog Variado</b>
    </h1>
    <br>
    <div class="container-fluid" style="margin-left: 8%; margin-right: 8%; background: rgb(244, 244, 244); border-radius: 2.3px; padding: 5px;" >
      <div class="row">
         <div class="col-sm-7">
           <h2 style="font-family: 'Architects Daughter', cursive; font-size: 3em;">¿De que trata esta pagina?</h2>
           <p>
             Blog variado es una pagina, para poder dar a conocer pensamientos, hasta anegdotas, en que tus aportes sera manejados en inicio como borradores, y luego poder publicarlos a todo tu publico, obten mayor numero de seguidores en tus entradas para poder ser mas famoso aca.
           </p>
         </div>
         <div class="col-sm-5">
           <img src="resources/img/blog-1.jpg" style="height: 300px; border-radius: 3px;" alt="Blog Variado Descrip">
         </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
           <img src="resources/img/seguridad-blog.jpg"  style="height: 270px; border-radius: 3px;" alt="Blog Seguridad Descrip">
        </div>
        <div class="col-sm-7">
          <h2 style="font-family: 'Architects Daughter', cursive; font-size: 3em;">¿Quíen puede ver mis entradas?</h2>
          <p >Todos los visitantes del sitio, ya que podran buscar tus entradas desde la barra de busqueda, y tendras subscriptores a quienes les llegaran primero tus ultimas entradas.
          </p>
        </div>
      </div>
      <div  class="row" style="margin-top: 3em;">
        <div class="col-sm-12">
          <h2 class="text-center" style="font-family: 'Josefin Slab' ;"><b>Crea tu perfil, comienza a publicar tus pensamientos y ve ganando suscriptores</b></h2>
          <img  src="resources/img/persona.jpg" style="width: 75%; margin-left: 15%; margin-rigth: 15%; margin-top: 2em; margin-bottom: 1em; border-radius: 2px;" alt="Persona 1">
          <div class="ladrillo-parallax" style="width: 100%;">
            <div class="texto-parallax" style="width: 100%;">

            <h3 class="text-center" style="  padding-top: 3em;
              border: solid 2px rgb(87, 214, 161);
              font-family: 'Playfair Display';
              padding-top: 0.5em;
              padding-bottom: 0.5em;
              font-size: 2em; font-weight: bold;
              text-shadow: 2px 2px rgb(0, 0, 0);" >Genera tendencias, dá tus conocimientos a tus suscritores</h3>
          </div>
        </div>

        </div>
      </div>
      <div class="row">

      </div>
    </div>
    <?php include 'view/plantillas/footer.php'; ?>
  </body>
</html>
