<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('America/Guatemala');
require_once '../model/conexion.php';
  function crearPost($post_data){
    $query = "INSERT INTO post(idCategoria, idUsuario, idEstadoP,  contenido, titulo,fecha)".
             "VALUES({$post_data['idCategoria']},{$post_data['idUsuario']},{$post_data['idEstadoP']},'{$post_data['contenido']}','{$post_data['titulo']}','{$post_data['fecha']}')";
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $conexion->query($query);
        return true;
      } catch (Exception $e) {
       return false;
      }
    }else {
      return false;
    }
  }
  function editarPost($post_data){
    $query = "UPDATE post SET idCategoria={$post_data['idCategoria']}, idUsuario={$post_data['idUsuario']}, idEstadoP={$post_data['idEstadoP']}, contenido ='{$post_data['contenido']}', titulo='{$postpost_data['titulo']}' WHERE idPost={$post_data['idPost']} ; ";
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $conexion->query($query);
        return true;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function eliminarPost($idPost){
    $query = "DELETE FROM post WHERE idPost={$idPost} ; ";
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $conexion->query($query);
        return true;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function mostrarPostUsuario($idUsuario,$estadoP){
    $query = "SELECT * , cat.categoria, us.nombre, us.nick, est.estado FROM post pt INNER JOIN categoria cat ON cat.idCategoria = pt.idCategoria INNER JOIN usuario us ON us.idUsuario = pt.idUsuario INNER JOIN estadoPost est ON est.idEstado = pt.idEstadoP WHERE pt.idUsuario =".$idUsuario." AND pt.idEstadoP=".$estadoP;
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $resultadoSQL= $conexion->query($query);
        $return;
        foreach ($resultadoSQL as $row){
          $return[] = $row;
        }
        return $return;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function mostrarPostUsuario5($idUsuario){
    $query = "(SELECT * , cat.categoria, us.nombre, us.nick, est.estado FROM post pt INNER JOIN categoria cat ON cat.idCategoria = pt.idCategoria INNER JOIN usuario us ON us.idUsuario = pt.idUsuario INNER JOIN estadoPost est ON est.idEstado = pt.idEstadoP WHERE pt.idUsuario =".$idUsuario." AND pt.idEstadoP=1 ORDER BY pt.fecha DESC  LIMIT 4  ) UNION
    (SELECT * , cat.categoria, us.nombre, us.nick, est.estado FROM post pt INNER JOIN categoria cat ON cat.idCategoria = pt.idCategoria INNER JOIN usuario us ON us.idUsuario = pt.idUsuario INNER JOIN estadoPost est ON est.idEstado = pt.idEstadoP WHERE pt.idUsuario =".$idUsuario." AND pt.idEstadoP=2 ORDER BY pt.fecha DESC  LIMIT 4 ) ;";
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $resultadoSQL= $conexion->query($query);
        $return;
        foreach ($resultadoSQL as $row){
          $return[] = $row;
        }
        return $return;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function busqueda($busqueda){
    $query ="SELECT pt.titulo, pt.contenido,us.nick,pt.fecha FROM post pt
            INNER JOIN usuario us ON us.idUsuario = pt.idUsuario
            WHERE pt.titulo LIKE '%".$busqueda."%'
            UNION
            (SELECT us.nombre,us.apellido,us.nick, rl.rol FROM usuario us INNER JOIN roles rl ON rl.idRol = us.idRol WHERE us.nick LIKE '%".$busqueda."%')";
            $conexion = iniciarConexion();
            if(isset($conexion)){
              try {
                $resultadoSQL= $conexion->query($query);
                $return;
                foreach ($resultadoSQL as $row){
                  $return[] = $row;
                }
                return $return;}
              catch (Exception $e) {
               return false;}
            }else {
              return false;}
  }
  function mostrarPostRecientes(){
    $query = "SELECT *,cat.categoria, us.nombre, us.nick, est.estado FROM post pt INNER JOIN categoria cat ON cat.idCategoria = pt.idCategoria INNER JOIN usuario us ON us.idUsuario = pt.idUsuario INNER JOIN estadoPost est ON est.idEstado = pt.idEstadoP ORDER BY fecha DESC ;" ;
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $resultadoSQL= $conexion->query($query);
        $return;
        foreach ($resultadoSQL as $row){
          $return[] = $row;
        }
        return $return;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function borradoraPost($idPost){
    $query = "UPDATE post SET idEstadoP = 1 WHERE idPost={$idPost} ; ";
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $conexion->query($query);
        return true;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }

  if($_POST['opcion']){
 switch ($_POST['opcion']){
      case "crear-p":
      $datos_post = array('idCategoria' => $_POST['idCategoria'],'idUsuario' => $_POST['idUsuario'],'idEstadoP' => $_POST['idEstadoP'],'contenido' => $_POST['contenido'], 'fecha' => $_POST['fecha'],'titulo' => $_POST['titulo']);
      echo json_encode(crearPost($datos_post));
      break;
      case "editar-p":
      $datos_post = array('idCategoria' => $_POST['idCategoria'],'idUsuario' => $_POST['idUsuario'],'idEstadoP' => $_POST['idEstadoP'],'contenido' => $_POST['contenido'], 'fecha' => $_POST['fecha'],'idPost' => $_POST['idPost'],'titulo' => $_POST['titulo'] );
      echo json_encode(editarPost($datos_post));
      break;
      case "eliminar-p":
      echo json_encode(eliminarPost($_POST['idPost']));
      break;
      case "mostrar-p-user":
      echo json_encode(mostrarPostUsuario($_POST['idUsuario'],$_POST['estadoP'] ));
      break;
      case "mostrar-p-user-5":
      echo json_encode(mostrarPostUsuario5($_POST['idUsuario']));
      break;
      case "mostrar-p-res":
      echo json_encode(mostrarPostRecientes());
      break;
      case "borrador-a-p":
      echo json_encode(borradoraPost($_POST['idPost']));
      break;
      case "busqueda":
      echo json_encode(busqueda($_POST['busqueda']));
      break;
    }
  }else {
    echo "Respuesta no encontrada";
  }
?>
