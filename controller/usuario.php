<?php
session_start();
require_once '../model/conexion.php';

  function Login($usuario){
    $query = "SELECT * FROM usuario WHERE nick='".$usuario['nick']."' AND contrasena='". $usuario['contrasena']."' ;";
    $conexion = iniciarConexion();
    if (isset($conexion)){
    $resultadoSQL = $conexion->query($query);
    $datosUser;
    foreach ($resultadoSQL as $row){
      $datosUser[] = $row;
    }
  }
  if (isset($datosUser)) {

    $_SESSION['datosUsuario']= $datosUser;
    return $datosUser;
  }else {
    return false;
  }
  }

  function getNicks($idUsuario){
    $query = "SELECT us.*, r.rol FROM usuario us INNER JOIN roles r ON r.idRol = us.idRol WHERE us.idUsuario !=".$idUsuario;
    $conexion = iniciarConexion();
    if (isset($conexion)){
    $resultadoSQL = $conexion->query($query);
    $datosUser;
    foreach ($resultadoSQL as $row){
      $datosUser[] = $row;
    }
  }
  if (isset($datosUser)) {

    return $datosUser;
  }else {
    return false;

  }
  }

  function agregarUsuario($data_user){
    $query = "INSERT INTO usuario(nombre,apellido,nick,contrasena,idRol)".
             "VALUES('{$data_user["nombre"]}','{$data_user["apellido"]}','{$data_user["nick"]}','{$data_user["contrasena"]}',{$data_user["idRol"]})";
     $conexion = iniciarConexion();
     if(isset($conexion)){
       try {
         $conexion->query($query);
         return true;
       }catch (Exception $e) {
         return false;}
     }else{
       return false;}
  }
  function editarUsuario($data_user){
    $query = "UPDATE usuario SET nombre= '{$data_user["nombre"]}' ".
             ",apellido ='{$data_user["apellido"]}',nick='{$data_user["nick"]}',contrasena= '{$data_user["contrasena"]}' ,idRol={$data_user["idRol"]} WHERE idUsuario = {$data_user['idUsuario']}";
   $conexion = iniciarConexion();
   if(isset($conexion)){
     try {
       $conexion->query($query);
       return true;
     }catch (Exception $e) {
       return false;}
   }else{
     return false;}
  }
  function eliminarUsuario($idUsuario){
    $query = "DELETE FROM usuario WHERE idUsuario= ".$idUsuario;
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $conexion->query($query);

        return true;
      }catch (Exception $e) {

        return false;}
    }else{
      return false;}
  }
  function unUsuario($data_user){
    $query;
    if(isset($data_user['idUsuario']) && $data_user['idUsuario'] != "" ){
      $query = "SELECT * FROM usuario WHERE idUsuario=".$data_user['idUsuario'];
    }elseif(isset($data_user['nick']) && $data_user['nick'] != "" ){
      $query = "SELECT * FROM usuario WHERE nick='".$data_user['nick']."' ;";
    }elseif(isset($data_user['nombre']) && $data_user['nombre'] != "" ){
      $query = "SELECT * FROM usuario WHERE nombre LIKE %'".$data_user['idUsuario'] ."'% ; ";
    }else {
        return false;
        break;
    }
    $conexion = iniciarConexion();
    if (isset($conexion)){
    $resultadoSQL = $conexion->query($query);
    $datosUser;
    foreach ($resultadoSQL as $row){
      $datosUser[] = $row;
    }
  }
  if (isset($datosUser)) {
    return $datosUser;
  }else {
    return false;
  }
  }

  function NoUsuarios($idUsuario){
    $query = "SELECT COUNT(nick) AS conteo FROM usuario us  WHERE us.idUsuario !=".$idUsuario;
    $conexion = iniciarConexion();
    if (isset($conexion)){
    $resultadoSQL = $conexion->query($query);
    $datosUser;
    foreach ($resultadoSQL as $row){
      $datosUser[] = $row;
    }
  }
  if (isset($datosUser)) {
    return $datosUser;
  }else {
    return false;
  }
  }

  if(isset($_POST['opcion'])){
 switch ($_POST['opcion']){
   case 'login':
   $usuario = ['nick'=>$_POST['nick'], 'contrasena'=> $_POST['contrasena']];
   $resultado = Login($usuario);
   if ( $resultado != null) {
     $datos_user = $resultado[0];
     switch ($datos_user['idRol']){
       case 1:
       $return = array('location' => "../view/home-users/home-admin.php", );
         echo json_encode($return);
         break;
       case 2:
        $return = array('location' => "../view/home-users/home-escritor.php", );
          echo json_encode($return);
         break;
      case 3:

       break;
       default:
         # code...
         break;
     }
   }else {
     $_SESSION['errorLogin'] = "Usuario o Contraseña incorrectos";
     $return = array('location' => false );
    echo json_encode($return);
   }
     break;
    case 'agregar-u':
      $data_user = array('nombre' => $_POST['nombre-u'],'apellido' => $_POST['apellido-u'],'nick' => $_POST['nick-u'], 'contrasena' => $_POST['passw-u'],'idRol' => $_POST['idRol-u'] );
      echo json_encode(agregarUsuario($data_user));
    break;
    case 'editar-u':
    $data_user = array('nombre' => $_POST['nombre-u'],'apellido' => $_POST['apellido-u'],'nick' => $_POST['nick-u'], 'contrasena' => $_POST['passw-u'],'idRol' => $_POST['idRol-u'],'idUsuario' => $_POST['id-u'] );
    echo json_encode(editarUsuario($data_user));
    break;
    case 'eliminar-u':
    echo json_encode(eliminarUsuario($_POST['id-u']));
    break;
    case 'unUsuario':
        $data_user = array('nombre' => (isset($_POST['nombre-u'])?$_POST['nombre-u'] :null),
        'nick' => (isset($_POST['nick-u'])?$_POST['nick-u'] :null) ,
        'idRol' =>(isset($_POST['idRol-u'])?$_POST['idRol-u'] :null) ,
        'idUsuario' =>(isset($_POST['id-u'])?$_POST['id-u'] :null)  );
        echo json_encode(unUsuario($data_user));
    break;
    case 'getNicks':
        echo json_encode(getNicks($_POST['idUsuario']));
    break;
    case 'NoUsuarios':
        echo json_encode(NoUsuarios($_POST['idUsuario']));
    break;
   default:
    echo "No se encontro opcion";
     break;
 }
  }else {
    echo "No se encontro respuesta";
  }

 ?>
