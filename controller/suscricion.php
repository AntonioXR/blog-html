<?php
require_once '../model/conexion.php';
  function suscribirUsuario($idUsuarioA,$idUsuarioB){
    $query="INSERT INTO suscriptores(idUsuarioPublica,idUsuarioLector) ".
           "VALUES({$idUsuarioA},{$idUsuarioB}) ;";
    $conexion = iniciarConexion();
    if(isset($conexion)){
     try {
       $conexion->query($query);
     return true;
         } catch (Exception $e) {
          return false;
         }
  }
}
  function misSuscripciones($idUsuarioLector){
    $query="SELECT (SELECT us.idUsuario FROM suscriptores sus INNER JOIN usuario us ON us.idUsuario = sus.idUsuarioPublica) AS 'idUsuario', (SELECT us.nombre FROM suscriptores sus INNER JOIN usuario us ON us.idUsuario = sus.idUsuarioPublica) AS 'nombre', (SELECT us.nick FROM suscriptores sus INNER JOIN usuario us ON us.idUsuario = sus.idUsuarioPublica) AS 'nick' FROM suscriptores WHERE idUsuarioLector=".$idUsuarioLector;
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $resultadoSQL= $conexion->query($query);
        $return;
        foreach ($resultadoSQL as $row){
          $return[] = $row;
        }
        return $return;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function misSuscritos($idUsuarioPublica){
    $query ="SELECT (SELECT us.idUsuario FROM suscriptores sus INNER JOIN usuario us ON us.idUsuario = sus.idUsuarioLector) AS 'idUsuario', (SELECT us.nombre FROM suscriptores sus INNER JOIN usuario us ON us.idUsuario = sus.idUsuarioLector) AS 'nombre', (SELECT us.nick FROM suscriptores sus INNER JOIN usuario us ON us.idUsuario = sus.idUsuarioLector) AS 'nick' FROM suscriptores  WHERE idUsuarioPublica=".$idUsuarioPublica;
    $conexion = iniciarConexion();
    if(isset($conexion)){
      try {
        $resultadoSQL= $conexion->query($query);
        $return;
        foreach ($resultadoSQL as $row){
          $return[] = $row;
        }
        return $return;}
      catch (Exception $e) {
       return false;}
    }else {
      return false;}
  }
  function desSuscribir($idUsuarioPublica,$idUsuarioLector){
    $query = "DELETE FROM suscriptores WHERE idUsuarioLector=".$idUsuarioLector." AND idUsuarioPublica=".$idUsuarioPublica." ;";
    $conexion = iniciarConexion();
    if(isset($conexion)){
     try {
       $conexion->query($query);
     return true;
         } catch (Exception $e) {
          return false;
       }
  }
  }
  if (isset($_POST['opcion'])){
    switch ($_POST['opcion']){
      case 'suscribir-u':
      json_encode(suscribirUsuario($_POST['idUsuarioA'], $_POST['idUsuarioB']));
        break;
      case 'desuscribir-u':
      json_encode(desSuscribir($_POST['idUsuarioA'],$_POST['idUsuarioB']));
        break;
      case 'mis-suscripciones':
      json_encode(misSuscripciones($_POST['idUsuarioLector']));
        break;
      case 'mis-suscriptores':
      json_encode(misSuscritos($_POST['idUsuarioPublica']));
        break;
      default:
        echo "Opcion no encontrada";
        break;
    }
  }else {
    echo "Opcion no encontrada";
  }
 ?>
