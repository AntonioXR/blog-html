<?php
  function useCURL($URL, $datos){
    $cURL = curl_init();
    curl_setopt($cURL, CURLOPT_URL, $URL);
    curl_setopt($cURL, CURLOPT_POST, true);
    curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cURL, CURLOPT_POSTFIELDS, http_build_query($datos,'', '&'));
    $resultado = curl_exec($cURL);
    curl_close($cURL);
    unset($cURL);
    return $resultado;
  }
 ?>
